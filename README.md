## 如何启动

### 第一步 安装node环境

### 第二步 建议先安装淘宝镜像 装一次永久生效 作用是加快依赖的安装速度 执行 npm config set registry https://registry.npm.taobao.org

### 第三步 查看镜像是否安装成功 执行 npm config get registry 如果打印结果是 https://registry.npm.taobao.org 进入下一步

### 第四步 进入当前文件根目录 执行npm install
 
### 第五步 执行npm start

### 第六步 浏览器打开 http://localhost:3000  地址访问


## 项目简介

##### React + three.js  前端框架是React, 在其中引入了three.js

##### 代码热更新的，更新后，刷新页面即可获取最新效果