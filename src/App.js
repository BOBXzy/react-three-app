import React, { useState } from 'react';
import Cube from './pages/Cube';
import Model from './pages/Model';
import './App.css';

const SelectControlPanel = () => {
  const [mode, setMode] = useState(0);
  return (
    <React.Fragment>
      {
        mode === 0 &&
        <div className="panel">
          <button onClick={() => { setMode(1); }}>正方体</button>
          <button onClick={() => { setMode(2); }}>模型</button>
          <button onClick={() => { console.log('待补充'); }}>3D地图</button>
        </div>
      }
      {
        mode === 1 ?
          <Cube />
          :
          mode === 2 ?
            <Model /> : null
      }
    </React.Fragment>
  );
};

export default function App() {
  return (
    <SelectControlPanel />
  );
}
